.PHONY = jenkins python-server


VAR_DIR = "variables"


jenkins:
	@terraform apply -var-file=variables/jenkins.tfvars


python-server:
	@terraform apply -var-file=variables/python_server.tfvars
