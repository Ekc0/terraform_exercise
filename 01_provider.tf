provider "google" {
	credentials = "${file("~/terraform_key.json")}"
	project = "qac-terraform"
	region = "europe-west2"
}
